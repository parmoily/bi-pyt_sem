Aplikace slouzi k praci nad obrazkami, muze cist a ukladat zmeneny obrazky.

Tato aplikace se ovlada pomoci prikazove radce a prikazu "python"

Pro spusteni aplikace:

\<.py skript name> [--\<flags> \<parameter>]

Popis funkce aplikace muzete zjistit pomoci prikazu:

\<.py skript name> -h

Funkce, ktere aplikace muze aplikovat na obrazek:
lighter, rotate, darkern, mirror, inverse, bw, sharpen

Priklad pouziti:
\<.py skript name> -i test_image.jpg -o last_test.jpg --mirror --sharpen