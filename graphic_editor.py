import sys
import getopt
from PIL import Image, ImageDraw
from enum import Enum


class ChangeType(Enum):
    ROTATE = 1
    MIRROR = 2
    INVERSE = 3
    BW = 4
    LIGHTEN = 5
    DARKEN = 6
    SAHRPEN = 7


def make_darker(in_draw, in_width, in_height, in_pix, in_val):
    print("makeDarker")
    if in_val > 0:
        for x in range(in_width):
            for y in range(in_height):
                rgb_val_by_percent = 255 / 100
                darker_val = rgb_val_by_percent * in_val
                r = in_pix[x, y][0]
                g = in_pix[x, y][1]
                b = in_pix[x, y][2]
                in_draw.point((x, y), (int(r - darker_val), int(g - darker_val), int(b - darker_val)))


def make_lighter(in_draw, in_width, in_height, in_pix, in_val):
    print("makeLighter")
    for x in range(in_width):
        for y in range(in_height):
            rgb_val_ny_percent = 255 / 100
            lighter_val = rgb_val_ny_percent * in_val
            r = in_pix[x, y][0]
            g = in_pix[x, y][1]
            b = in_pix[x, y][2]
            in_draw.point((x, y), (int(r + lighter_val), int(g + lighter_val), int(b + lighter_val)))


def make_bw(in_draw, in_width, in_height, in_pix):
    print("make_bw")
    for x in range(in_width):
        for y in range(in_height):
            r = in_pix[x, y][0]
            g = in_pix[x, y][1]
            b = in_pix[x, y][2]
            sr = (r + g + b) // 3
            in_draw.point((x, y), (sr, sr, sr))


def make_inverse(in_draw, in_width, in_height, in_pix):
    print("make_inverse")
    for x in range(in_width):
        for y in range(in_height):
            r = in_pix[x, y][0]
            g = in_pix[x, y][1]
            b = in_pix[x, y][2]
            in_draw.point((x, y), (255 - r, 255 - g, 255 - b))


def make_rotate(in_width, in_height, in_pix):
    print("make_rotate")
    tmp_image = Image.new('RGB', (in_height, in_width))
    tmp_draw = ImageDraw.Draw(tmp_image)
    for x in range(in_width):
        for y in range(in_height):
            r = in_pix[x, y][0]
            g = in_pix[x, y][1]
            b = in_pix[x, y][2]
            tmp_draw.point(((in_height - 1) - y, x), (r, g, b))
    return tmp_image


def make_mirror(in_width, in_height, in_pix):
    print("make_mirror")
    tmp_image = Image.new('RGB', (in_width, in_height))
    tmp_draw = ImageDraw.Draw(tmp_image)
    for x in range(in_width):
        for y in range(in_height):
            r = in_pix[x, y][0]
            g = in_pix[x, y][1]
            b = in_pix[x, y][2]
            tmp_draw.point(((in_width - 1) - x, y), (r, g, b))
    return tmp_image


# algorithm from Wikipedia
def make_sharpen(in_width, in_height, in_pix, in_karnel_type, in_sharpen_val):
    print("make_sharpen")
    tmp_image = Image.new('RGB', (in_width, in_height))
    tmp_draw = ImageDraw.Draw(tmp_image)
    sharpen_val = in_sharpen_val

    default_karnel = [[0, -1 * sharpen_val, 0],
                      [-1 * sharpen_val, (4 * sharpen_val + 1), 0],
                      [0, -1 * sharpen_val, 0]]

    hard_karnel = [[-1 * sharpen_val, -1 * sharpen_val, -1 * sharpen_val],
                   [-1 * sharpen_val, (8 * sharpen_val) + 1, -1 * sharpen_val],
                   [-1 * sharpen_val, -1 * sharpen_val, -1 * sharpen_val]]

    super_hard_karnel = \
        [[0, -1 * sharpen_val, -1 * sharpen_val, -1 * sharpen_val, 0],
         [-1 * sharpen_val, -1 * sharpen_val, -1 * sharpen_val, -1 * sharpen_val, -1 * sharpen_val],
         [-1 * sharpen_val, -1 * sharpen_val, (24 * sharpen_val) + 1, -1 * sharpen_val, -1 * sharpen_val],
         [-1 * sharpen_val, -1 * sharpen_val, -1 * sharpen_val, -1 * sharpen_val, -1 * sharpen_val],
         [0, -1 * sharpen_val, -1 * sharpen_val, -1 * sharpen_val, 0]]

    if in_karnel_type == 0:
        margin = 1
        curr_karnel = default_karnel
    elif in_karnel_type == 1:
        margin = 1
        curr_karnel = hard_karnel
    elif in_karnel_type == 2:
        margin = 2
        curr_karnel = super_hard_karnel
    else:
        sys.exit(2)

    for x in range(margin, in_width - margin):
        for y in range(margin, in_height - margin):
            new_pixel_value_r: float = 0
            new_pixel_value_g: float = 0
            new_pixel_value_b: float = 0

            for xk in range(- margin, margin + 1):
                for yk in range(- margin, margin + 1):
                    curr_r = in_pix[xk + x, yk + y][0]
                    curr_g = in_pix[xk + x, yk + y][1]
                    curr_b = in_pix[xk + x, yk + y][2]
                    new_pixel_value_r += curr_karnel[xk + margin][yk + margin] * curr_r
                    new_pixel_value_g += curr_karnel[xk + margin][yk + margin] * curr_g
                    new_pixel_value_b += curr_karnel[xk + margin][yk + margin] * curr_b

            tmp_draw.point((x, y), (int(new_pixel_value_r), int(new_pixel_value_g), int(new_pixel_value_b)))
    return tmp_image


def main(argv):
    inputfile = ''
    outputfile = ''
    lighten: int = 0
    darken: int = 0
    sharpen_type: int = 2
    sharpen_val: float = 0.05
    operations = []

    try:
        opts, args = getopt.getopt(argv, "hl:d:i:o:",
                                   ["rotate", "mirror", "bw", "inverse", "lighten", "darken", "sharpen", "shType=",
                                    "shVal="])
    except getopt.GetoptError:
        print('Wrong execution format: try -h to view command description !!!')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('\n -i <inputfile> \n -o <outputfile>')
            print(' -l <value> -- make image lighter by <value>% ')
            print(' -d <value> -- make image darkern by <value>% ')
            print(' --rotate -- rotate the image 90 degrees to the right')
            print(' --mirror -- mirror image on the vertical axis')
            print(' --inverse -- invert the colors of each pixel')
            print(' --bw -- convert image to black and white')
            print(' --sharpen -- apply unsharp mask to the image')
            print('       --shType -- type of unsharp mask; int value from 0 to 2')
            print('       --shVal -- mask application power; float value from 0 to 1')
            sys.exit()
        elif opt == "--rotate":
            operations.append(ChangeType.ROTATE)
        elif opt == "--mirror":
            operations.append(ChangeType.MIRROR)
        elif opt == '--inverse':
            operations.append(ChangeType.INVERSE)
        elif opt == "--bw":
            operations.append(ChangeType.BW)
        elif opt == "-l":
            operations.append(ChangeType.LIGHTEN)
            lighten = int(arg)
        elif opt == "-d":
            operations.append(ChangeType.DARKEN)
            darken = int(arg)
        elif opt == "--sharpen":
            operations.append(ChangeType.SAHRPEN)
        elif opt == "--shType":
            if (int(arg) < 0) or (int(arg) > 2):
                print("wrong --shType parameter")
                exit(2)
            sharpen_type = int(arg)
        elif opt == "--shVal":
            if (float(arg) < 0) or (float(arg) > 1):
                print("wrong --shVal parameter")
                exit(2)
            sharpen_val = float(arg)
        elif opt == "-i":
            inputfile = arg
        elif opt == "-o":
            outputfile = arg
    print('Input file is ', inputfile)
    print('Output file is ', outputfile)

    image = Image.open(inputfile)
    draw = ImageDraw.Draw(image)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()

    for it in operations:
        if it == ChangeType.ROTATE:
            image = make_rotate(width, height, pix)
            draw = ImageDraw.Draw(image)
            width = image.size[0]
            height = image.size[1]
            pix = image.load()
        elif it == ChangeType.MIRROR:
            image = make_mirror(width, height, pix)
            draw = ImageDraw.Draw(image)
            width = image.size[0]
            height = image.size[1]
            pix = image.load()
        elif it == ChangeType.INVERSE:
            make_inverse(draw, width, height, pix)
        elif it == ChangeType.BW:
            make_bw(draw, width, height, pix)
        elif it == ChangeType.LIGHTEN:
            make_lighter(draw, width, height, pix, lighten)
        elif it == ChangeType.DARKEN:
            make_darker(draw, width, height, pix, darken)
        elif it == ChangeType.SAHRPEN:
            image = make_sharpen(width, height, pix, sharpen_type, sharpen_val)
            draw = ImageDraw.Draw(image)
            width = image.size[0]
            height = image.size[1]
            pix = image.load()

    image.save(outputfile, "JPEG")


if __name__ == "__main__":
    main(sys.argv[1:])
