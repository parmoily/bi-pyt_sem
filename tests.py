import pytest
import graphic_editor
from PIL import Image, ImageDraw


def compare_pixels(in_pix_1, in_pix_2, w, h):
    equal = True
    for x in range(w):
        for y in range(h):
            f = in_pix_1[x, y]
            s = in_pix_2[x, y]
            if f != s:
                equal = False
    return equal


def test_rotation():

    print('Test start')
    image_orig = Image.open("test_image.jpg")
    width_orig = image_orig.size[0]
    height_orig = image_orig.size[1]
    pix_orig = image_orig.load()

    image = Image.open("test_image.jpg")
    width = image.size[0]
    height = image.size[1]

    for i in range(0, 4):
        image = graphic_editor.make_rotate(width, height, image.load())
        width = image.size[0]
        height = image.size[1]

    assert compare_pixels(pix_orig, image.load(), width_orig, height_orig)


def test_mirror():

    print('Test start')
    image_orig = Image.open("test_image.jpg")
    width_orig = image_orig.size[0]
    height_orig = image_orig.size[1]
    pix_orig = image_orig.load()

    image = Image.open("test_image.jpg")
    width = image.size[0]
    height = image.size[1]

    for i in range(0, 2):
        image = graphic_editor.make_mirror(width, height, image.load())
        width = image.size[0]
        height = image.size[1]

    assert compare_pixels(pix_orig, image.load(), width_orig, height_orig)


def test_inverse():

    print('Test start')
    image_orig = Image.open("test_image.jpg")
    width_orig = image_orig.size[0]
    height_orig = image_orig.size[1]
    pix_orig = image_orig.load()

    image = Image.open("test_image.jpg")
    width = image.size[0]
    height = image.size[1]
    draw = ImageDraw.Draw(image)

    for i in range(0, 2):
        graphic_editor.make_inverse(draw, width, height, image.load())
        width = image.size[0]
        height = image.size[1]
        draw = ImageDraw.Draw(image)

    assert compare_pixels(pix_orig, image.load(), width_orig, height_orig)
